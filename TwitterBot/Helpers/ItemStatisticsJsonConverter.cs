﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using TwitterBot.Model;

namespace TwitterBot.Helpers
{
    public class ItemStatisticsJsonConverter : JsonConverter
    {
        public override bool CanRead
        {
            get { return false; }
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            var itemStatistics = (List<ItemStatistics>)value;

            var result = new JObject();
            foreach (var item in itemStatistics)
            {
                result.Add(item.Item.ToString(), item.Rate.ToString(CultureInfo.InvariantCulture));
                //result.Add(item.Item.ToString(), item.Rate);
            }
            result.WriteTo(writer);

        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            throw new NotImplementedException();
        }

        public override bool CanConvert(Type objectType)
        {
            return (objectType == typeof(List<ItemStatistics>));
        }
    }
}
