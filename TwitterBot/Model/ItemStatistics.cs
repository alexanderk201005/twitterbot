﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TwitterBot.Model
{
    public class ItemStatistics
    {
        public char Item { get; set; }
        public int Count { get; set; }
        public decimal Rate { get; set; }
    }
}
