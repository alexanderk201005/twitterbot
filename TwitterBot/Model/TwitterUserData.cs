﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tweetinvi.Models;

namespace TwitterBot.Model
{
    public class TwitterUserData
    {
        public IUser User { get; set; }
        public IUserIdentifier UserIdentifier { get; set; }
    }
}
