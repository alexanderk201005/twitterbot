﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Autofac;
using TwitterBot.Code;
using TwitterBot.Helpers;
using TwitterBot.Interfaces;

namespace TwitterBot
{
    class Program
    {
        private static IContainer Container { get; set; }

        static void Main(string[] args)
        {
            try
            {
                var builder = new ContainerBuilder();

                builder.RegisterType<JsonItemStatisticsConverter>().As<IItemStatisticsConverter>().SingleInstance();
                builder.RegisterType<ItemStatisticsCreator>().As<IItemStatisticsCreator>().SingleInstance();
                builder.RegisterType<MainLayer>().As<IMainLayer>().SingleInstance();
                builder.RegisterType<TwitterLayer>().As<ITwitterLayer>().SingleInstance();
                builder.RegisterType<TwitterItemStatisticsReader>().As<IItemStatisticsReader>().SingleInstance();
                builder.RegisterType<TwitterItemStatisticsWriter>().As<TwitterItemStatisticsWriter>().SingleInstance();

                Container = builder.Build();

                var mainLayer = Container.Resolve<IMainLayer>();

                if (mainLayer.Init())
                {
                    mainLayer.Run();
                }
                else
                {
                    Console.WriteLine(@"Нажмите любую клавишу для выхода...");
                    Console.ReadKey();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(@"ОШИБКА: '{0}'", ex.Message);
                Console.WriteLine(@"Нажмите любую клавишу для выхода...");
                Console.ReadKey();
            }
        }
    }
}
