﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TwitterBot.Interfaces;
using TwitterBot.Model;

namespace TwitterBot.Code
{
    public class ItemStatisticsCreator : IItemStatisticsCreator
    {
        public IEnumerable<ItemStatistics> GetFrequencyOfLetters(string input)
        {
            if (string.IsNullOrEmpty(input))
            {
                return new List<ItemStatistics>();
            }

            var filtered = new string(input.ToLower()
                .Where(char.IsLetter)
                .ToArray());

            var filteredLength = filtered.Length;

            var result = filtered
                .GroupBy(c => c)
                .Select(g => new ItemStatistics
                {
                    Item = g.Key,
                    Count = g.Count(),
                    Rate = Math.Round((decimal)g.Count() / filteredLength * 100M, 4, MidpointRounding.AwayFromZero)
                })
                .OrderBy(i => i.Item);

            return result;
        }

    }
}
