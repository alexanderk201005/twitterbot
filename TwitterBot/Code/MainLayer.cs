﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TwitterBot.Interfaces;

namespace TwitterBot.Code
{
    public class MainLayer : IMainLayer
    {
        private readonly IItemStatisticsConverter _itemConverter;
        private readonly IItemStatisticsCreator _itemCreator;
        private readonly ITwitterLayer _twitterLayer;
        private readonly IItemStatisticsReader _twitterReader;
        private readonly TwitterItemStatisticsWriter _twitterWriter;

        public MainLayer(
            IItemStatisticsConverter itemConverter,
            IItemStatisticsCreator itemCreator,
            ITwitterLayer twitterLayer,
            IItemStatisticsReader twitterReader,
            TwitterItemStatisticsWriter twitterWriter
            )
        {
            _itemConverter = itemConverter;
            _itemCreator = itemCreator;
            _twitterLayer = twitterLayer;
            _twitterReader = twitterReader;
            _twitterWriter = twitterWriter;
        }

        public bool Init()
        {
            var authorized = _twitterLayer.Authorize();

            if (authorized) return true;

            Console.WriteLine(@"ОШИБКА: Не удалось авторизовать приложение.");
            return false;
        }

        public void Run()
        {
            while (true)
            {
                Console.Write(@"Введите имя пользователя или нажмите Ввод для выхода:");
                var line = Console.ReadLine();

                if (line == string.Empty)
                {
                    return;
                }

                ProccessUser(line);
            }
        }

        private void ProccessUser(string input)
        {
            if(string.IsNullOrEmpty(input)) return;

            var data = _twitterReader.GetData(input);

            if (data == null) return;

            var itemStatisticses = _itemCreator.GetFrequencyOfLetters(string.Concat(data));

            var json = _itemConverter.Convert(itemStatisticses);

            var result = input + ", статистика для последних 5 твитов: "
                              + Environment.NewLine + json;

            Console.WriteLine(result);

            _twitterWriter.Write(result);
        }
    }
}
