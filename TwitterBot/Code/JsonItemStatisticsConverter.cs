﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using TwitterBot.Helpers;
using TwitterBot.Interfaces;
using TwitterBot.Model;

namespace TwitterBot.Code
{
    public class JsonItemStatisticsConverter : IItemStatisticsConverter
    {
        public string Convert(IEnumerable<ItemStatistics> statisticses)
        {
            if (statisticses == null)
            {
                return string.Empty;
            }

            var jsonSerializer = new JsonSerializer();
            jsonSerializer.Converters.Add(new ItemStatisticsJsonConverter());
            jsonSerializer.Formatting = Formatting.Indented;

            var sb = new StringBuilder();
            using (var sw = new StringWriter(sb))
            using (var writer = new JsonTextWriter(sw))
            {
                //writer.QuoteName = false;
                jsonSerializer.Serialize(writer, statisticses.ToList());
            }

            return sb.ToString();
        }
    }
}
