﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.NetworkInformation;
using System.Text;
using System.Threading.Tasks;
using Tweetinvi;
using Tweetinvi.Logic;
using TwitterBot.Interfaces;
using TwitterBot.Helpers;

namespace TwitterBot.Code
{
    public class TwitterItemStatisticsWriter : IItemStatisticsWriter
    {
        private readonly ITwitterLayer _twitterLayer;

        private const int _tweetLength = 280;

        public TwitterItemStatisticsWriter(ITwitterLayer twitterLayer)
        {
            _twitterLayer = twitterLayer;
        }

        public void Write(string output)
        {
            if(string.IsNullOrEmpty(output)) return;

            WriteMessage(output);

            WriteTweets(output);
        }

        private void WriteTweets(string text)
        {
            var list = Preprocess(text);

            var tweet = _twitterLayer.PublishTweet(list.First());

            if (tweet == null)
            {
                Console.WriteLine(@"ОШИБКА: Твит не создан.");
                return;
            }

            Console.WriteLine(@"Твит создан: " + tweet.Id);

            list.RemoveAt(0);

            if (list.Any())
            {
                foreach (var item in list)
                {
                    tweet = _twitterLayer.PublishTweetInReplyTo(item, tweet);

                    if (tweet == null)
                    {
                        Console.WriteLine(@"ОШИБКА: Твит не создан.");
                        return;
                    }

                    Console.WriteLine(@"Твит создан: " + tweet.Id);
                }
            }
        }

        private void WriteMessage(string text)
        {
            var authenticatedUser = Tweetinvi.User.GetAuthenticatedUser();

            if (authenticatedUser != null)
            {
                var msg = _twitterLayer.PublishMessage(text, authenticatedUser.ScreenName);

                if (msg == null)
                {
                    Console.WriteLine(@"ОШИБКА: Сообщение не отправлено.");
                    return; 
                }

                Console.WriteLine(@"Сообщение отправлено: " + msg.Id);
            }
            else
            {
                Console.WriteLine(@"ОШИБКА: Не удалось получить авторизованного пользователя.");
            }
        }

        private List<string> Preprocess(string input)
        {
            return input.Length <= _tweetLength ? new List<string>() {input} : input.Split(_tweetLength).ToList();
        }
    }
}
