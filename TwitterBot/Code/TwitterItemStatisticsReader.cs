﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TwitterBot.Interfaces;

namespace TwitterBot.Code
{
    public class TwitterItemStatisticsReader : IItemStatisticsReader
    {
        private readonly ITwitterLayer _twitterLayer;

        public TwitterItemStatisticsReader(ITwitterLayer twitterLayer)
        {
            _twitterLayer = twitterLayer;
        }

        public List<string> GetData(string input)
        {
            if (string.IsNullOrEmpty(input))
            {
                Console.WriteLine(@"ОШИБКА: Некорректное значение input.");
                return null;
            }

            var userData = _twitterLayer.GetUser(input);

            if (userData == null)
            {
                Console.WriteLine(@"Пользователь не существует.");
                return null;
            }

            var tweets = _twitterLayer.GetUserTweets(userData.UserIdentifier);

            if (tweets == null)
            {
                Console.WriteLine(@"ОШИБКА: Не удалось получить твиты пользователя");
                return null;
            }

            var tweetsList = tweets.ToList();

            if (!tweetsList.Any())
            {
                Console.WriteLine(@"У пользователя нет твитов");
                return null;
            }

            return tweetsList.Select(t => t.Text).ToList();
        }
    }
}
