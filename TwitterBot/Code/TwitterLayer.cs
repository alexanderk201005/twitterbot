﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tweetinvi;
using Tweetinvi.Models;
using Tweetinvi.Parameters;
using Tweetinvi.Core.Extensions;
using Tweetinvi.Exceptions;
using TwitterBot.Interfaces;
using TwitterBot.Model;
using TwitterBot.Properties;

namespace TwitterBot.Code
{
    public class TwitterLayer : ITwitterLayer
    {
        public TwitterLayer()
        {
            TweetinviConfig.CurrentThreadSettings.TweetMode = TweetMode.Extended;
            TweetinviConfig.ApplicationSettings.TweetMode = TweetMode.Extended;

            ExceptionHandler.SwallowWebExceptions = false;
        }

        public bool Authorize()
        {
            try
            {
                var consumerKey = Settings.Default.consumerKey;
                var consumerSecret = Settings.Default.consumerSecret;

                if (consumerKey == null || consumerSecret == null)
                {
                    Console.WriteLine(@"ОШИБКА: Значения CONSUMER_KEY и/или CONSUMER_SECRERT не заданы.");
                    return false;
                }

                var appCredentials = new TwitterCredentials(consumerKey, consumerSecret);

                var authenticationContext = AuthFlow.InitAuthentication(appCredentials);

                Process.Start(authenticationContext.AuthorizationURL);

                Console.Write(@"Введите PIN:");

                var pinCode = Console.ReadLine();

                var userCredentials = AuthFlow.CreateCredentialsFromVerifierCode(pinCode, authenticationContext);

                if (userCredentials == null)
                {
                    return false;
                }

                Auth.SetCredentials(userCredentials);

                return true;

            }
            catch (ArgumentException ex)
            {
                Console.WriteLine(@"ОШИБКА: Недопустимые параметры запроса '{0}'", ex.Message);
            }
            catch (TwitterException ex)
            {
                if (ex.StatusCode == 401)
                {
                    Console.WriteLine(@"ОШИБКА: Неверный PIN.");
                    return false;
                }
                Console.WriteLine(@"ОШИБКА: Что-то пошло не так, когда мы попытались выполнить HTTP-запрос : '{0}'", ex.TwitterDescription);
            }
            catch (Exception ex)
            {
                Console.WriteLine(@"ОШИБКА: '{0}'", ex.Message);
            }

            return false;
        }

        public TwitterUserData GetUser(string userName)
        {
            if (string.IsNullOrEmpty(userName))
            {
                Console.WriteLine(@"ОШИБКА: Некорректное значение имени пользователя.");
                return null;
            }

            var firstChar = userName.Substring(0, 1);

            var userScreenName = string.Copy(userName);

            if (firstChar == "@")
            {
                userScreenName = userName.Substring(1, userName.Length - 1);
            }

            try
            {
                var user = User.GetUserFromScreenName(userScreenName);

                if (user == null)
                {
                    return null;
                }

                var userIdentifier = new UserIdentifier(userScreenName);

                var twitterUserData = new TwitterUserData()
                {
                    User = user,
                    UserIdentifier = userIdentifier
                };

                return twitterUserData;
            }
            catch (ArgumentException ex)
            {
                Console.WriteLine(@"ОШИБКА: Недопустимые параметры запроса '{0}'", ex.Message);
            }
            catch (TwitterException ex)
            {
                Console.WriteLine(@"ОШИБКА: Что-то пошло не так, когда мы попытались выполнить HTTP-запрос : '{0}'", ex.TwitterDescription);
            }

            return null;
        }

        public IEnumerable<ITweet> GetUserTweets(IUserIdentifier userIdentifier,
            int maximumNumberOfTweetsToRetrieve)
        {
            if (userIdentifier == null)
            {
                Console.WriteLine(@"ОШИБКА: Некорректное значение идентификатора пользователя.");
                return null;
            }

            var userTimelineParameters = new UserTimelineParameters()
            {
                MaximumNumberOfTweetsToRetrieve = maximumNumberOfTweetsToRetrieve
            };

            try
            {
                return Timeline.GetUserTimeline(userIdentifier, userTimelineParameters);
            }
            catch (ArgumentException ex)
            {
                Console.WriteLine(@"ОШИБКА: Недопустимые параметры запроса '{0}'", ex.Message);
            }
            catch (TwitterException ex)
            {
                Console.WriteLine(@"ОШИБКА: Что-то пошло не так, когда мы попытались выполнить HTTP-запрос : '{0}'", ex.TwitterDescription);
            }

            return null;
        }

        public IMessage PublishMessage(string text, string userName)
        {
            if (string.IsNullOrEmpty(text))
            {
                Console.WriteLine(@"ОШИБКА: Некорректное значение text.");
                return null;
            }

            if (string.IsNullOrEmpty(userName))
            {
                Console.WriteLine(@"ОШИБКА: Некорректное значение имени пользователя.");
                return null;
            }

            try
            {
                var message = Message.PublishMessage(text, userName);

                if (message != null && message.IsMessagePublished)
                {
                    return message;
                }
            }
            catch (ArgumentException ex)
            {
                Console.WriteLine(@"ОШИБКА: Недопустимые параметры запроса '{0}'", ex.Message);
            }
            catch (TwitterException ex)
            {
                Console.WriteLine(@"ОШИБКА: Что-то пошло не так, когда мы попытались выполнить HTTP-запрос : '{0}'", ex.TwitterDescription);
            }

            return null;
        }

        public ITweet PublishTweet(string text)
        {
            if (string.IsNullOrEmpty(text))
            {
                Console.WriteLine(@"ОШИБКА: Некорректное значение text.");
                return null;
            }

            try
            {
                return Tweet.PublishTweet(text);
            }
            catch (ArgumentException ex)
            {
                Console.WriteLine(@"ОШИБКА: Недопустимые параметры запроса '{0}'", ex.Message);
            }
            catch (TwitterException ex)
            {
                if (ex.TwitterExceptionInfos != null && ex.TwitterExceptionInfos.Any() && ex.TwitterExceptionInfos.First().Code == 187)
                {
                    Console.WriteLine(@"ОШИБКА: Дубликат сообщения.");
                    return null;
                }
                Console.WriteLine(@"ОШИБКА: Что-то пошло не так, когда мы попытались выполнить HTTP-запрос : '{0}'", ex.TwitterDescription);
            }

            return null;
        }

        public ITweet PublishTweetInReplyTo(string text, ITweet tweetToReply)
        {
            if (string.IsNullOrEmpty(text))
            {
                Console.WriteLine(@"ОШИБКА: Некорректное значение text.");
                return null;
            }

            if (tweetToReply == null)
            {
                Console.WriteLine(@"ОШИБКА: Некорректное значение tweetToReply.");
                return null;
            }

            if (!tweetToReply.IsTweetPublished)
            {
                Console.WriteLine(@"ОШИБКА: tweetToReply должен быть опубликован.");
                return null;
            }

            try
            {
                return Tweet.PublishTweet(text, new PublishTweetOptionalParameters
                {
                    InReplyToTweet = tweetToReply,
                    //AutoPopulateReplyMetadata = true
                });
            }
            catch (ArgumentException ex)
            {
                Console.WriteLine(@"ОШИБКА: Недопустимые параметры запроса '{0}'", ex.Message);
            }
            catch (TwitterException ex)
            {
                if (ex.TwitterExceptionInfos != null && ex.TwitterExceptionInfos.Any() && ex.TwitterExceptionInfos.First().Code == 187)
                {
                    Console.WriteLine(@"ОШИБКА: Дубликат сообщения.");
                    return null;
                }
                Console.WriteLine(@"ОШИБКА: Что-то пошло не так, когда мы попытались выполнить HTTP-запрос : '{0}'", ex.TwitterDescription);
            }

            return null;
        }
    }
}
