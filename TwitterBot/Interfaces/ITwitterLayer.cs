﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tweetinvi.Models;
using TwitterBot.Model;

namespace TwitterBot.Interfaces
{
    public interface ITwitterLayer
    {
        bool Authorize();

        TwitterUserData GetUser(string userName);

        IEnumerable<ITweet> GetUserTweets(IUserIdentifier userIdentifier,
            int maximumNumberOfTweetsToRetrieve = 5);

        IMessage PublishMessage(string text, string userName);

        ITweet PublishTweet(string text);

        ITweet PublishTweetInReplyTo(string text, ITweet tweetToReply);
    }
}
