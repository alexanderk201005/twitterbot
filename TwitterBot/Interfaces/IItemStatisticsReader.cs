﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TwitterBot.Interfaces
{
    public interface IItemStatisticsReader
    {
        List<string> GetData(string input);
    }
}
