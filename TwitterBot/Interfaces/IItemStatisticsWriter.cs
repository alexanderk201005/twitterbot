﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TwitterBot.Interfaces
{
    interface IItemStatisticsWriter
    {
        void Write(string output);
    }
}
