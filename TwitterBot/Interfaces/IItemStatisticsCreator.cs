﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TwitterBot.Model;

namespace TwitterBot.Interfaces
{
    public interface IItemStatisticsCreator
    {
        IEnumerable<ItemStatistics> GetFrequencyOfLetters(string input);
    }
}
